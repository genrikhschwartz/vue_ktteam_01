const itemAvailable = [5, 3, 3];

let availableInTimer = document.querySelectorAll('.equipment__available-value');
let availableInDesc = document.querySelectorAll('.equipment__available-current');

const instantDays = 23;
const instantHours = 8;
const instantMinutes = 59;

let timerBlock = document.querySelectorAll('.timer');

function isSingle(el) {
  if (el < 10) {
    el = "0" + el;
  }

  return el;
}

function countDown(elDays, elHours, elMinutes) {
  let days = parseInt(elDays.textContent, 10);
  let hours = parseInt(elHours.textContent, 10);
  let minutes = parseInt(elMinutes.textContent, 10);
  
  if (!days && !hours && !minutes) {
    for (let i = 1; i < 999; i++) {
      window.clearInterval(i);
    }
    return;
  }

  minutes--;
  if (minutes === -1) {
    hours--;
    minutes = 59;
  }
  if (hours === -1) {
    days--;
    hours = 23;
  }
  
  elDays.textContent = isSingle(days);
  elHours.textContent = isSingle(hours);
  elMinutes.textContent = isSingle(minutes);
}

for (let i = 0; i < timerBlock.length; i++) {
  let days = timerBlock[i].querySelector('.equipment__days');
  let hours = timerBlock[i].querySelector('.equipment__hours');
  let minutes = timerBlock[i].querySelector('.equipment__minutes');

  days.textContent = isSingle(instantDays);
  hours.textContent = isSingle(instantHours);
  minutes.textContent = isSingle(instantMinutes);

  setInterval(() => { countDown(days, hours, minutes) }, 6);
}

for (let i = 0; i < itemAvailable.length; i++) {
  let availTimer = availableInTimer[i];
  let availDesc = availableInDesc[i];

  availTimer.textContent = itemAvailable[i] + ' шт';
  availDesc.textContent = itemAvailable[i] + ' шт';
}