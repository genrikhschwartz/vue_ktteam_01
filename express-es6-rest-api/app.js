const express = require('express');
const request = require('request');
const cors = require('cors');
const app = express();
const config = require('./config.json');

app.use(cors());

const PORT = config.port;
const apiServerHost = config.elasticUrl;

app.get('/catalog', (req, res) => {
  res.send('Hello World!');
})

app.use('/catalog/:index/_search', (req, res) => {

  if ( ! (req.method == 'GET' || req.method == 'POST') ) {
    errMethod = { error: req.method + " request method is not supported. Use GET or POST." };
    console.log("ERROR: " + req.method + " request method is not supported.");
    res.status(405).send("Method is not supported.");
    res.end();
    return;
  }

  let reqUser = req.url;
  let getParam = reqUser.slice(1);
  let url = apiServerHost + req.params.index + '/_search' + getParam; 
    req.pipe(request({uri: url}, (err, res, body) => {
      if (body.includes('\"error\"')) {
        let errorES = JSON.parse(body);
        console.log(errorES);
      } 
    })).pipe(res);
});

app.listen(PORT, () => {
  console.log(`На этом порту сервер - ${PORT}`);
  console.log(`А здесь живёт эластиксёрч - ${apiServerHost}`);
});