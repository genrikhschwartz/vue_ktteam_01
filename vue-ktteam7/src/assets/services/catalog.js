export default function getItems(queryParam = 'q=*&sort=_id:asc') {
  const url = 'http://127.0.0.1:8079/catalog/products/_search?';
  const endpoint = url + queryParam;
  const xhr = new XMLHttpRequest;
  let result;
  xhr.onreadystatechange = () => {
    if (xhr.readyState === XMLHttpRequest.DONE) {
      result = JSON.parse(xhr.responseText);
      return result;
    }
  };
  xhr.open('GET', endpoint, false);
  xhr.send();
  return result;
};