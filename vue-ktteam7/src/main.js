import Vue from 'vue'
import Vuex from 'vuex'
import EquipmentList from './Equipment-list.vue'
import getItems from './assets/services/catalog'

Vue.use(Vuex);

let result = getItems();

const store = new Vuex.Store({
  state: {
    catalog: result.hits.hits,
    days: 23,
    hours: 56,
    minutes: 30,
    show: true
  },
  mutations: {
    countDown(state) {
      if (!state.days && !state.hours && !state.minutes) {
        for (let i = 1; i < 999; i++) {
          window.clearInterval(i);
        }
        return;
      }
      state.minutes--;
      if (state.minutes === -1) {
        state.hours--;
        state.minutes = 59;
      }
      if (state.hours === -1) {
        state.days--;
        state.hours = 23;
      }
    },
    checkTimer(state) {
      if (!state.days && !state.hours && !state.minutes) {
        for (let i = 1; i < 999; i++) {
          window.clearInterval(i);
        }
        return;
      }
    },
    displayModal(state) {
      state.show = true;
      console.log(state.show);
    },
    hideModal(state) {
      state.show = false;
    }
  },
  actions: {
    letGo(context) {
      setInterval(() => {
        context.commit('countDown')
      }, 60000)
    }
  }
});

Vue.component('equipmentList', EquipmentList);

let products = result.hits.hits;
const app = new Vue({
  el: '#app',
  store,
  render: (h) => h(EquipmentList)
});


